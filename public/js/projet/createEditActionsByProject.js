/**
 * Jquerry init document
 */
 jQuery(document).ready(function() {
    var $collectionHolderAction;
    
    //setup button for add new action
    var $addActionButton = $('<button type="button" class="add_action_link btn btn-info mt-2">Ajouter une action</button>');
    var $newLinkActionLi = $('<li class="list-group-item"></li>').append($addActionButton);
    
    //Get the ul that holds the collection of action
    $collectionHolderAction = $('ul.actions');

    //add delete link to all of the existing action form li elements
    $collectionHolderAction.find('li').each(function() {
        addActionFormDelete($(this));
    });

    //add the "add action" anchor and li to the ul
    $collectionHolderAction.append($newLinkActionLi);
    $collectionHolderAction.append($addActionButton);

    //count the current form inputs
    $collectionHolderAction.data('index', $collectionHolderAction.find(':input').length);

    //add eventListener click
    $addActionButton.on('click', function(e) {
        addActionForm($collectionHolderAction, $newLinkActionLi);
    });
});

/**
 * Add new action to the form
 * @param {*} $collectionHolderAction 
 * @param {*} $newLinkActionLi 
 */
function addActionForm($collectionHolderAction, $newLinkActionLi) 
{
    var prototype = $collectionHolderAction.data('prototype');

    var index = $collectionHolderAction.data('index');

    var newForm = prototype;

    newForm = newForm.replace(/__name__/g, index);

    $collectionHolderAction.data('index', index + 1);

    var $newFormLi = $('<li class="action list-group-item"></li>').append(newForm);

    addActionFormDelete($newFormLi);

    $newLinkActionLi.before($newFormLi);
}

/**
 * Add delete Button for a specific li
 * @param {*} $actionFormLi 
 */
function addActionFormDelete($actionFormLi) {
    var $removeFormButton = $('<button type="button" class="btn btn-danger mt-2">Supprimer cette action</button>');
    //var $removeFormButton = $('<div class="col-sm-0 from-group align-self-center"><a href="#"><img class="trashLink align-item-center" src="../../../img/trash.svg"  width="30" height="30" alt="trash"/></a></div>');
    $actionFormLi.children().prepend($removeFormButton);

    $removeFormButton.on('click', function(e) {
        $actionFormLi.remove();
    });
}


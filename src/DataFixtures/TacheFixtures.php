<?php

namespace App\DataFixtures;

use App\Entity\Tache;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class TacheFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $this->generateTache(20, $manager);

        $manager->flush();
    }

    public function getDependencies()
    {
        return[
            LotFixtures::class
        ];
    }

    private function generateTache(int $number, ObjectManager $manager): void
    {
        for($i = 0; $i < $number; $i++)
        {
            $task = new Tache();

            $task->setNumero($i+1)
                 ->setLot($this->getReference("lot". mt_rand(0, 9)))
            ;

            $this->addReference("task{$i}", $task);

            $manager->persist($task);
        }
    }
}

<?php

namespace App\DataFixtures;

use App\Entity\Lot;
use App\DataFixtures\ProjetFixtures;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class LotFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $this->generateLots(10, $manager);

        $manager->flush();
    }

    public function getDependencies()
    {
        return[
            ProjetFixtures::class
        ];
    }

    private function generateLots(int $number, ObjectManager $manager): void
    {
        for($i = 0; $i < $number; $i++)
        {
            $lot = new Lot();

            $lot->setNumero($i+1)
                ->setProjet($this->getReference("project". mt_rand(0, 4)))
            ;

            $this->addReference("lot{$i}", $lot);

            $manager->persist($lot);
        }
    }
}

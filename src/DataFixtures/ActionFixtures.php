<?php

namespace App\DataFixtures;

use App\Entity\Action;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ActionFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $this->generateActions(40, $manager);

        $manager->flush();
    }

    public function getDependencies()
    {
        return[
            TacheFixtures::class
        ];
    }

    private function generateActions(int $number, ObjectManager $manager): void
    {
        for($i = 0; $i < $number; $i++)
        {
            $action = new Action();

            $action->setDesignation('Action'. $i+1)
                ->setTache($this->getReference("task". mt_rand(0, 19)))
            ;

            $manager->persist($action);
        }
    }
}

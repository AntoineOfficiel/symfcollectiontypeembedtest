<?php

namespace App\DataFixtures;

use App\Entity\Projet;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProjetFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $this->generateProject(5, $manager);

        $manager->flush();
    }

    private function generateProject(int $number, ObjectManager $manager): void
    {
        for($i = 0; $i < $number; $i++)
        {
            $project = new Projet();

            $project->setDesignation('projet '.$i+1);

            $this->addReference("project{$i}", $project);

            $manager->persist($project);
        }
    }
}

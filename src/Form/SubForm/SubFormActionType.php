<?php

namespace App\Form\SubForm;

use App\Entity\Tache;
use App\Entity\Action;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SubFormActionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('designation', TextType::class, [
                'label'    => 'désignation',
                'mapped'   => true,
                'required' => true,
                'attr'     => [
                    'class' => 'form-control'
                ]
            ]) 
            ->add('tache', EntityType::class, [
                'class'        => Tache::class,
                'choice_value' => 'id',
                'choice_label' => 'numero',
                'placeholder'  => 'tache',
                'label'        => 'Tache',
                'mapped'       => true,
                'required'     => true,
                'attr'         => [
                    'class' => 'form-control'
                ]
            ])   
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Action::class,
        ]);
    }
}

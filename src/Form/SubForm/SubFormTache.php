<?php

namespace App\Form\SubForm;

use App\Entity\Lot;
use App\Entity\Tache;
use App\Form\SubForm\SubFormActionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class SubFormTacheType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lot', EntityType::class, [
                'class'        => Lot::class,
                'choice_label' => 'numero',
                'choice_value' => 'id',
                'placeholder'  => 'Lot',
                'label'        => 'Lot',
                'mapped'       => true,
                'attr'         => [
                    'class' => 'form-control'
                ]
            ])
            ->add('actions', CollectionType::class, [
                'allow_add'     => true,
                'allow_delete'  => true,
                'entry_options' => [
                    'label' => false
                ],
                'entry_type'    => SubFormActionType::class,
                'prototype'     => true,
                'by_reference'  => false,
                'mapped'        => true,
                'required'      => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tache::class,
        ]);
    }
}

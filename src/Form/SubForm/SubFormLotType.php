<?php

namespace App\Form\SubForm;

use App\Entity\Lot;
use App\Form\SubForm\SubFormTacheType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class SubFormLotType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('taches', CollectionType::class, [
                'allow_add'    => true,
                'allow_delete' => true,
                'entry_options' => [
                    'label' => false,
                ],
                'entry_type'   => SubFormTacheType::class,
                'prototype'    => true,
                'mapped'       => true,
                'required'     => true,
                'by_reference' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Lot::class,
        ]);
    }
}

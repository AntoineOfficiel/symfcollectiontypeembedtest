<?php

namespace App\Form;

use App\Entity\Projet;
use App\Form\SubForm\SubFormLotType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;


class ActionsByProjetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('designation', TextType::class, [
                'label'    => 'designation',
                'mapped'   => true,
                'required' => true,
                'disabled' => true,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('lots', CollectionType::class, [
                'allow_add'     => true,
                'allow_delete'  => true,
                'entry_options' => [ 
                    'label' => false,
                ],
                'entry_type'    => SubFormLotType::class,
                'prototype'     => true,
                'mapped'        => true,
                'by_reference'  => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Projet::class,
        ]);
    }
}

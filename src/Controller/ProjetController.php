<?php

namespace App\Controller;

use App\Entity\Projet;
use App\Form\ActionsByProjetType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProjetController extends AbstractController
{

    /**
     * @Route("/projet/show/{id<\d+>}", name="app_show_projet")
     */
    public function showProject(Projet $project = null): Response
    {
        if(!$project)
        {
            throw new \Exception('Erreur lors de la récupération');
        }

        return $this->render('projet/projet.html.twig', [
            'projet' => $project
        ]);
    }

    /**
     * @Route("/projet/createactions/{id<\d+>}", name="app_create_actions_from_project")
     */
    public function createActionsFromProjet(Projet $project = null, Request $request, EntityManagerInterface $entityManager): Response
    {
        if(!$project)
        {
            throw new \Exception('Erreur lors de la récupération');
        }

        $form = $this->createForm(ActionsByProjetType::class, $project);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {

            $entityManager->flush();

            dd('stop');
            return $this->redirectToRoute('app_fiche_projet', [
                'id' => $project->getId() 
            ]);
        }

        return $this->render('projet/actionByProjet.html.twig', [
            'form'    => $form->createView(),
        ]);
    }
}
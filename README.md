# Symfony project: SymfCollectionTypeEmbedTest

## Short presentation
I need help for imbricate CollectionType.  

The problem:  
Bonjour, dans le cadre d'un projet de gestion de projet je bloque sur une formulaire avec des CollectionType imbriqué
J'ai bessoin de créer/modifier/supprimer des actions a partir d'un projet. L'utilisateur doit pouvoir selectionner un lot et une tache pour chaque nouvelle action.
Lors de l'envoi du formulaire seul les actions peuvent etre créer/modifié/supprimé (la relation avec tache aussi).

Sachant que dans la BDD, un projet possède des lots. Un lot contient des taches. Une tache contient des actions

Dans un précedent formulaire avec un seul CollectionType j'arrive a le faire fonctionner mais pas avec plusieurs CollectionType imbriqué.
Après avoir essayé pas mal de chose je n'y arrive pas.
Avez vous une idée pour faire fonctionner les CollectionType ? OU avez vous une autre solution ?

## Project Environment
OS: Fedora 34 
PHP version: 8
Symfony version: Symfony 5
Composer version: 2.1.8  

Base de donnée: mysql

## Run project
For running this project :  
* symfony server:start or symfony serve -d or php -S localhost:8000-> for run php server (you need symfony console)  